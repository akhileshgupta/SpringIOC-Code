package test;



import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import beans.Test;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	
		
		/*ApplicationContext ap=new ClassPathXmlApplicationContext();
    
		    Test t=(Test)ap.getBean("t");
	
	    
		      t.Hello("Akhilesh");
	*/
		
		 Resource r=new ClassPathResource("resources/spring.xml");  
	        BeanFactory factory=new XmlBeanFactory(r);  
	          
	        Test s=(Test)factory.getBean("t");  
	        s.Hello("Akhilesh");  
	
	

	}

}
