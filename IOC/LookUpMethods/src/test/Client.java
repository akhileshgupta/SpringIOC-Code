package test;



import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import beans.Bus;
import beans.Car;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	
		
		ApplicationContext ap=new ClassPathXmlApplicationContext("resource/spring.xml");
		
		System.out.println("------Car---------");
		
		Car c=(Car)ap.getBean("c");
		System.out.println(c.myCarEngine().getName());

		System.out.println("-------------bus------");
		Bus b=(Bus)ap.getBean("b");
		System.out.println(b.getClass().getCanonicalName());
		System.out.println(b.myBusEngine().getName());
		
	}

}
