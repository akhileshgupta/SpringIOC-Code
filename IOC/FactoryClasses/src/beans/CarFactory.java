package beans;

import org.springframework.beans.factory.config.MethodInvokingFactoryBean;

//static Approach
//So no need to implmented any static method

public class CarFactory {
	
	
	
	//MethodInvokingFactoryBean
	
	
	public static String carname;
	public static void setCarname(String carname) {
		CarFactory.carname = carname;
	}
	
	public static Car getInstance() throws Exception{
		
	Car c=(Car)	Class.forName(carname).newInstance();
		
	
	return c;
		
	}

}
