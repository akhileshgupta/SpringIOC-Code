package beans;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Test {

	String driver,url,username,pwd;
	
private Connection conn;
	
	

public void setDriver(String driver) {
	this.driver = driver;
}

public void setUrl(String url) {
	this.url = url;
}

public void setUsername(String username) {
	this.username = username;
}

public void setPwd(String pwd) {
	this.pwd = pwd;
}

public void setConn(Connection conn) {
	this.conn = conn;
}

public Connection getConn() {
	return conn;
}

@PostConstruct
public void conInit() throws Exception {
		// TODO Auto-generated method stub
		
		Class.forName(driver);
		 conn=DriverManager.getConnection(url, username, pwd);
		System.out.println("Connection Open.....");
	}
	
	public void saveData(int id,String name,String email,String address)
	throws Exception{
		
		PreparedStatement ps=conn.prepareStatement("insert into values(?,?,?,?,?)");
		ps.setInt(1, id);
		ps.setString(2, name);
		ps.setString(3, email);
		ps.setString(4, address);
		ps.executeUpdate();
		System.out.println("Insertion Success....");
	}
	

	@PreDestroy
	public void conCleanup() throws Exception {
		
		conn.close();
		System.out.println("Connection Close...");
		
		
	}
	
	
	

}
