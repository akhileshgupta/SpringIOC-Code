package test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Cars;

public class Client {

	public static void main(String[] args) {
	
    
		String files[]=new String[]{"resources/car.xml","resources/engine.xml"};
		
		
		ApplicationContext ap= new ClassPathXmlApplicationContext(files);

   
		 Cars c=(Cars)ap.getBean("c");
		 c.Display();
	}

}
