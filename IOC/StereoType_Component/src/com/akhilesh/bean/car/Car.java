package com.akhilesh.bean.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.akhilesh.bean.engine.Engine;


@Component
public class Car {

	
	@Autowired
	private Engine engine;
	
	public void Display(){
		
		System.out.println("Engine Name:"+engine.getEngine());
	}
}
