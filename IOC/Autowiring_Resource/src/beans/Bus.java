package beans;

public class Bus {
	
	private Engine engine;
	
	public Bus(Engine engine) {
		
		System.out.println("Parametrised Constructor of Bus");
		this.engine=engine;
		
	}
	
	public void DisplayBus(){
	    
		 System.out.println("Bus ModelYear is:"+engine.getModelyear());
	
	}
	}