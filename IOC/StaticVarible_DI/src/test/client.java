package test;

import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Car;

public class client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//It is used for invoc the Static method and used in Xml file and then Stop
		//MethodInvokingFactoryBean m=new MethodInvokingFactoryBean();
		//m.setArguments(arguments);
		//m.setStaticMethod(staticMethod);

		ApplicationContext ap= new ClassPathXmlApplicationContext("resources/spring.xml");
		
		Car car=(Car)ap.getBean("c");
		car.Display();
		
	}

}
