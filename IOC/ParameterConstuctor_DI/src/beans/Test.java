package beans;

public class Test {
	
	
	private String name;
	private int age;
	private String email;
	
	public Test() {		
		System.out.println("This is Private "
				+ "constructor Of Test...");

			}
	
	public Test(String name,int age,String email)
	{
		this();
		this.name=name;
		this.age=age;
		this.email=email;
		
	}	

	
	public void Display(){
		
		System.out.println("Name:"+name);
		System.out.println("Age:"+age);
		System.out.println("Email:"+email);
	}
}
