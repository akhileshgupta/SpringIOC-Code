package test;



import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import beans.Test;

public class Client {

	public static void main(String[] args) {
	

		 Resource r=new ClassPathResource("resources/spring.xml");  
	        BeanFactory factory=new XmlBeanFactory(r);  
	          
	        Test test=(Test)factory.getBean("t");  
	        test.Display(); 
	
	

	}

}
